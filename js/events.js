/**
 * Created by gatiss on 7/12/14.
 */
var events = {
    /*
    plus icons functionality
     */
    plus: function(){
        var self = this;
        var template = templates('button');
        var node = Creator(template);
        var parent = self.parentNode.parentNode.appendChild(node);
    },
    /*
    remove buttons functionality
     */
    minus: function(){
        var self = this;
        var container = self.parentNode.parentNode.parentNode;
        container.parentNode.removeChild(container);
    },
    /*
    clear text functionality
     */
    textAction: {
        onclick: function(){
            var self = this;
            if(self.value !== ""){
                self.value = "";
            }
        }
    },
    /*
    save functionality
     */
    save: function(){
        var self = this;
        if(self.textContent.toLowerCase() === 'save state'){
            if(core.saveLocalData()){
                self.setAttribute('class','save active');
                self.textContent = "Saving...";
                setTimeout(function(){
                    self.setAttribute('class','save');
                    self.textContent = "Successfully saved!";
                    setTimeout(function(){
                        self.textContent = "Save state";
                    },1000);
                },1000);
            };
        }
    }
}