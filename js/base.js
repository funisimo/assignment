/**
 * Created by gatis on 7/11/14.
 */
var core = {
    /*
    config holds the default configuration for object - mostly selectors for Sizzle
     */
    config:{
        container: ".container",
        content: ".container .content",
        buttons: {
            plus: ".container .plus",
            minus: ".container .minus",
            save: '.save',
            text: '.container .text'
        }
    },
    init: function(){
        /*
        check required files
         */
        if(typeof templates === 'undefined' || typeof events === 'undefined'){
            console.error('templates not loaded');
            return false;
        }
        /*
        self used to avoid problems in IE environment instead of "this"
         */
        var self = this;
        /*
         Object for debugging purposes
         usage - document.debug.push(message,arg1,arg2,argN)
         */
        document.debug = [];
        document.debug.push('initialized with config:',self.config);
        /*
           load data from localStorage if available
        */
        var data = self.loadLocalData();
        if(data !== false){
            self.load.content(data,self.config);
            self.apply.functionality(self.config);
        }else{
            self.initialize();
        }
        self.bindSaveLoadAction(self.config);
        return true;
    },
    /*
    load initial content for the first time
     */
    initialize:function(){
        var self = this;
        var config = self.config;
        var target = Sizzle(config.content)[0];
        var template = templates('button','initial');
        return Creator(target,template);
    },
    /*
    add action to save button
     */
    bindSaveLoadAction: function(config){
        if(typeof config !== 'object'){
            return false;
        }
        if(typeof config.buttons !== 'object'){
            return false;
        }
        var target = Sizzle(config.buttons.save)[0];
        if(Sizzle(config.buttons.save).length > 0){
            target.onclick = events.save;
            return true;
        }
        return false;

    },
    apply: {
        /*
        add actions to remove edit and add buttons
         */
        functionality: function(config){
            var self = this;
            if(typeof config !== 'object'){
                return false;
            }
            if(typeof config.buttons !== 'object'){
                return false;
            }
            var buttons = {
                pluses: Sizzle(config.buttons.plus),
                minuses: Sizzle(config.buttons.minus),
                text: Sizzle(config.buttons.text)
            }
            for(var id in buttons.pluses){
                var item = buttons.pluses[id];
                item.onclick = events.plus;
            }
            for(var id in buttons.minuses){
                var item = buttons.minuses[id];
                item.onclick = events.minus;
            }
            for(var id in buttons.text){
                var item = buttons.text[id];
                item.onclick = events.textAction.onclick;
            }
            return true;
        }
    },
    /*
    load data into container from local storage
     */
    load:{
        content: function(data,config){
            if(typeof config !== 'object' || typeof data !== 'object'){
                return false;
            }
            var target = Sizzle(config.content)[0];
            target.textContent = "";
            return Creator(target,data);
        }
    },
    /*
    save data to local storage
     */
    saveLocalData: function(){
        JF.templates.new ={};
        Sizzle(core.config.content)[0].normalize();
        Creator.indexHtml(Sizzle(core.config.content)[0],'new');
        var data = JF.templates.new.template;
        var self = this;
        /*
         check if feature available
         */
        if(typeof(Storage) !== "undefined") {
            try{
                localStorage.removeItem('jf');
                var string = JSON.stringify(data).replace('\\n','').replace('\\n','');
                localStorage.setItem('jf',string);
                return true;
            }catch(error){
                document.debug.push('saving error',error);
            }
        }
        return false;
    },
    /*
    load data from local storage
     */
    loadLocalData: function(){
        var self = this;
        /*
        check if feature available
         */
        if(typeof(Storage) !== "undefined") {
            if(localStorage.key('jf') !== null){
                try{
                    var data = JSON.parse(localStorage.getItem('jf'));
                    return data;
                }catch(error){
                    document.debug.push('parsing error',error);
                }
            }
        }
        return false;
    }

}
document.onreadystatechange = function(){
    if(document.readyState === 'complete'){
        core.init();
    }
}