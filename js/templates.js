/**
 * Created by gatiss on 7/11/14.
 */
/*
templates function creation to avoid symlinks of javascript
returns always new instance
 */
var templates = function($template,initial){
    if(typeof $template !== 'string'){
        return false;
    }
    var tmp = new Object({});
    /*
    new button template for JF framework
     */
    tmp['button'] = {
        element: "div",
        id: "button",
        class: "initial-container",
        wrapper:{
            element: "div",
            class: "wrapper",
            plus:{
                element: "img",
                class: "plus",
                src: "./img/plus.png",
                onclick: events.plus
            },
            content:{
                element: "div",
                class: "text-content",
                text: {
                    element: "input",
                    type: "text",
                    class: "text",
                    defaultValue: "Click to rename",
                    onclick: events.textAction.onclick
                },
                minus: typeof initial === 'undefined' ? {
                    element: "img",
                    class: "minus",
                    src: "./img/minus.png",
                    onclick: events.minus
                } : ""
            }
        }
    }

    if(typeof tmp[$template] !== 'undefined'){
        return tmp[$template];
    }else{
        return false;
    }
}