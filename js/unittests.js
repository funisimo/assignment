/**
 * Created by gatiss on 7/12/14.
 */
/*
whitebox testing
 */
document.runTests = function(){
    Sizzle('.unittests')[0].style.display = 'block';
    QUnit.test( "base tests", function( assert ) {
        assert.strictEqual( core.init(), true, "init completed successfully" );
        assert.strictEqual( core.initialize(), true, "initialize function" );
        assert.deepEqual(
            core.config,
            {
        container: ".container",
        content: ".container .content",
        buttons: {
            plus: ".container .plus",
            minus: ".container .minus",
            save: '.save',
            text: '.container .text'
        }
    },
            "configuration testing"
        );
        var config = core.config;
        assert.strictEqual( core.bindSaveLoadAction(config), true, "bindSaveLoadAction completed successfully" );
        assert.strictEqual( core.bindSaveLoadAction(), false, "bindSaveLoadAction failed successfully" );
        assert.strictEqual( core.apply.functionality(config), true, "apply completed successfully" );
        assert.strictEqual( core.apply.functionality(), false, "apply failed successfully" );
        if(localStorage.hasOwnProperty('jf')){
            assert.ok( core.loadLocalData(), typeof 'object', "returns object" );
            var data = core.loadLocalData();
            assert.strictEqual( core.load.content(data, config), true, "load content completed successfully" );
            assert.strictEqual( core.saveLocalData(), true, "save completed successfully" );
        }
        

    });
    QUnit.test( "templates tests", function( assert ) {
        assert.deepEqual(
            templates('button'),
            {
        element: "div",
        id: "button",
        class: "initial-container",
        wrapper:{
            element: "div",
            class: "wrapper",
            plus:{
                element: "img",
                class: "plus",
                src: "./img/plus.png",
                onclick: events.plus
            },
            content:{
                element: "div",
                class: "text-content",
                text: {
                    element: "input",
                    type: "text",
                    class: "text",
                    defaultValue: "Click to rename",
                    onclick: events.textAction.onclick
                },
                minus: {
                    element: "img",
                    class: "minus",
                    src: "./img/minus.png",
                    onclick: events.minus
                }
            }
        }
    },
            "template with minus button"
        );
        assert.deepEqual(
            templates('button',"init"),
            {
        element: "div",
        id: "button",
        class: "initial-container",
        wrapper:{
            element: "div",
            class: "wrapper",
            plus:{
                element: "img",
                class: "plus",
                src: "./img/plus.png",
                onclick: events.plus
            },
            content:{
                element: "div",
                class: "text-content",
                text: {
                    element: "input",
                    type: "text",
                    class: "text",
                    defaultValue: "Click to rename",
                    onclick: events.textAction.onclick
                },
                minus: ""
            }
        }
    },
            "template wthout minuse button"
        );
        assert.strictEqual( templates('casdcasd'), false, "no template" );
        assert.strictEqual( templates(), false, "no template name provided" );
        assert.strictEqual( templates({"yello":"testing"}), false, "accepting only string" );
    });
}
